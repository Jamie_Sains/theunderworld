using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public static AudioClip SwordAttack, GunShot, Walking, EnemyDeath, EnemyAttack, Music;
    static AudioSource AudioSource;

    private void Start()
    {
        SwordAttack = Resources.Load<AudioClip>("SwordAttack");
        GunShot = Resources.Load<AudioClip>("GunShot");
        Walking = Resources.Load<AudioClip>("Walking");
        EnemyDeath = Resources.Load<AudioClip>("EnemyDeath");
        EnemyAttack = Resources.Load<AudioClip>("EnemyAttack");
        Music = Resources.Load<AudioClip>("Music");

        AudioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "Sword":
                AudioSource.PlayOneShot(SwordAttack);
                break;
            case "Gun":
                AudioSource.PlayOneShot(GunShot);
                break;
            case "Walk":
                AudioSource.PlayOneShot(Walking);
                break;
            case "EnemyDeath":
                AudioSource.PlayOneShot(EnemyDeath);
                break;
            case "EnemyAttack":
                AudioSource.PlayOneShot(EnemyAttack);
                break;
            case "Music":
                AudioSource.PlayOneShot(Music);
                break;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            SoundManager.PlaySound("Walk");
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            SoundManager.PlaySound("Walk");
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            SoundManager.PlaySound("Walk");
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            SoundManager.PlaySound("Walk");
        }

        HealthManager HealthManScript;
        HealthManScript = GetComponent<HealthManager>();

    }
}
