using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    public Text timerText;
    public Text highScore;
    public bool timerActive;
    public float timeTaken;
    public float sceneBestTime = 60f;

    private void Start()
    {
        highScore.text = PlayerPrefs.GetFloat("HighScore", 0).ToString();
        sceneBestTime = PlayerPrefs.GetFloat("CurrentBestTime", sceneBestTime);
        timerActive = true;
    }

    public void update()
    {
        if (timerActive == true)
        {
            timeTaken += Time.deltaTime;
            timerText.text = timeTaken.ToString();
            Debug.Log("timer started");
        }
    }

    public void StopTimer()
    {
        Debug.Log("timer Stoped");
        timerActive = false;
        if(timeTaken < sceneBestTime)
        {
            highScore.text = timeTaken.ToString();

            PlayerPrefs.SetFloat("CurrentBestTime", timeTaken);
            PlayerPrefs.SetFloat("HighScore", timeTaken);
            Debug.Log("timer Stoped");
        }
    }
}
