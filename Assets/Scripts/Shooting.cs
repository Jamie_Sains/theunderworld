using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject BulletPrefab;

    public float bulletFlorce = 20f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
            SoundManager.PlaySound("Gun");
        }


    }

    void Shoot()
    {
       GameObject BulletUp = Instantiate(BulletPrefab, firePoint.position, firePoint.rotation);
       Rigidbody2D rb = BulletUp.GetComponent<Rigidbody2D>();
       rb.AddForce(firePoint.up * bulletFlorce, ForceMode2D.Impulse);
    }
}
